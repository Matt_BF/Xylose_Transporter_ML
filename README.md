# A machine learning model for xylose transporter prediction

If 2G ethanol is to become a viable technology as an alternative biofuel, it is necessary to obtain industrial yeast strains that are able to co-ferment hexoses and pentoses (especially xylose), and that are robust against fermentation inhibitors (such as HMF and acetic acid).

One major bottleneck for 2G fermentation is the insufficient knowledge of xylose transporters, and the ones that have already been described are highly repressed by glucose. Fast sugar transport is essential to increase ethanol productivity and yield.

Taking this challenge into account, one of my PhD's goals was to create a machine learning model that may predict the capabilities of a given transporter protein to transport xylose.

We opted to filter only fungi and bacteria sequences, but for a future model we plan on adding info from more organisms with known xylose transporters (e.g *Arabidopsis thaliana*).

Link for Biotech for Biofuels paper explaining all the work done https://biotechnologyforbiofuels.biomedcentral.com/articles/10.1186/s13068-022-02153-7


### Files explanation
#### Dataframes and matrices
- ***all_prots_uniprot.tab*** --> Tab-separated file obtained from Uniprot when searching for the MFS Sugar Porter Family

- ***all_transporters.csv*** --> Dataframe containing all information extracted from `all_prots_uniprot.tab` and filtered for sugars of interest. Dataframe created with `Uniprot_transporters.ipynb`

- ***all_transporters_sugar_value.csv*** --> Dataframe obtained from `df_prepare.ipynb`

- ***full_features.csv*** --> Final dataframe containing information extracted through `R_features.R` and joining relevant columns from `all_transporters_sugar_value.csv`

#### Bio files
- ***all_prots.fasta*** --> FASTA file obtained from `df_prepare.ipynb`. The header contains the Uniprot entry, the organism's genus and part of the protein name

- ***xylhp_interpro.gff*** --> GFF for xylhp, which was used later to isolate non-cytoplasmic domains for HMM feature

- ***fungi_bac_non_cito_xyl.promal.aln*** --> FASTA alignment file for creating the HMM

- ***HMM_non_cito_xyl*** --> HMM file for feature extraction

#### Model files
- ***xgb_rfecv_features.pickle*** --> Model ran for feature selection

- ***xgb_model.pickle*** --> Final fitted model file

### Notebooks explanation
- ***scrape_cbs.ipynb*** --> scrapes CBS database for info on sugar transport

- ***isolate_non_cito.ipynb*** --> uses info from `xylhp_interpro.gff` to isolate non cytoplasmic domains from the xylose-positive samples to create the HMM

- ***Uniprot_transporters.ipynb*** --> First notebook to run. Filters data from `all_prots_uniprot.tab`

- ***df_prepare.ipynb*** --> Second notebook to run. For working with data from `all_transporters.csv`. Extra cleanup and adds some more features to the data, such as adding binary values for each sugar, removing sequences with unreadable aminoacids, and manually adding sequences described in literature. Also, creation of fasta files for bioinformatic analysis, such as Orthofinder and BLAST searches

- ***machine_model.ipynb*** --> Third notebook to run. Model training, feature selection, dealing with imbalance, model testing, etc

- ***validate_ml/classify_transporters.ipynb*** --> Notebook that uses the model to classify the comparative genomics transporter dataset

### Scripts
- ***R_features.R*** --> R script containing code for feature extraction from protein sequences' FASTA file `all_prots.fasta`

