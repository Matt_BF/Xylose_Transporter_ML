library("protr")
#library("BioSeqClass")

args = commandArgs(trailingOnly=TRUE)

seqs <- readFASTA(args[1])

TC <- lapply(seqs, extractTC)
TC <- as.data.frame(do.call(rbind, TC))
rownames(TC) <- names(seqs)

Moreau <- lapply(seqs, extractMoreauBroto)
Moreau <- as.data.frame(do.call(rbind, Moreau))
rownames(Moreau) <- names(seqs)

APAAC <- lapply(seqs, extractAPAAC)
APAAC <- as.data.frame(do.call(rbind, APAAC))
rownames(APAAC) <- names(seqs)

BLOSUM <- lapply(seqs, extractBLOSUM, submat = "AABLOSUM62", k = 5, lag=7)
BLOSUM <- as.data.frame(do.call(rbind, BLOSUM))
rownames(BLOSUM) <- names(seqs)

pssmmat = lapply(seqs,extractPSSM,database.path=args[1])

PSSMAcc <- lapply(pssmmat, extractPSSMAcc,lag=7)
PSSMAcc <- as.data.frame(do.call(rbind, PSSMAcc))
rownames(PSSMAcc) <- names(seqs)


PSSMFeature <- lapply(pssmmat,extractPSSMFeature)
PSSMFeature <- as.data.frame(do.call(rbind, PSSMFeature))
rownames(PSSMFeature) <- names(seqs)

DescScales <- lapply(seqs, extractDescScales,propmat="AATopo",pc=5,lag=7)
DescScales <- as.data.frame(do.call(rbind, DescScales))
rownames(DescScales) <- names(seqs)

features <- cbind(TC, Moreau, APAAC, BLOSUM, PSSMAcc, PSSMFeature)
write.table(features, 'R_features.tsv', col.names = NA, quote = FALSE, sep = "\t")
