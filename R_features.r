library("protr")
#library("BioSeqClass")

seqs <- readFASTA("fungi_bac.fasta")

AAC <- lapply(seqs, extractAAC)
AAC <- as.data.frame(do.call(rbind, AAC))
rownames(AAC) <- names(seqs)

DC <- lapply(seqs,extractDC)
DC <- as.data.frame(do.call(rbind, DC))
rownames(DC) <- names(seqs)

TC <- lapply(seqs, extractTC)
TC <- as.data.frame(do.call(rbind, TC))
rownames(TC) <- names(seqs)

Moreau <- lapply(seqs, extractMoreauBroto)
Moreau <- as.data.frame(do.call(rbind, Moreau))
rownames(Moreau) <- names(seqs)

Moran <- lapply(seqs, extractMoran)
Moran <- as.data.frame(do.call(rbind, Moran))
rownames(Moran) <- names(seqs)

Geary <- lapply(seqs, extractGeary)
Geary <- as.data.frame(do.call(rbind, Geary))
rownames(Geary) <- names(seqs)


CTDC <- lapply(seqs,extractCTDC)
CTDC <- as.data.frame(do.call(rbind, CTDC))
rownames(CTDC) <- names(seqs)

CTDT <- lapply(seqs,extractCTDT)
CTDT <- as.data.frame(do.call(rbind, CTDT))
rownames(CTDT) <- names(seqs)

CTDD <- lapply(seqs,extractCTDD)
CTDD <- as.data.frame(do.call(rbind, CTDD))
rownames(CTDD) <- names(seqs)

SOCN <- lapply(seqs, extractSOCN)
SOCN <- as.data.frame(do.call(rbind, SOCN))
rownames(SOCN) <- names(seqs)

QSO <- lapply(seqs, extractQSO)
QSO <- as.data.frame(do.call(rbind, QSO))
rownames(QSO) <- names(seqs)

CTriad <- lapply(seqs, extractCTriad)
CTriad <- as.data.frame(do.call(rbind, CTriad))
rownames(CTriad) <- names(seqs)

PAAC <- lapply(seqs, extractPAAC)
PAAC <- as.data.frame(do.call(rbind, PAAC))
rownames(PAAC) <- names(seqs)

APAAC <- lapply(seqs, extractAPAAC)
APAAC <- as.data.frame(do.call(rbind, APAAC))
rownames(APAAC) <- names(seqs)

pssmmat <- lapply(seqs,extractPSSM,database.path="fungi_bac.fasta")

PSSMAcc <- lapply(pssmmat, extractPSSMAcc,lag=7)
PSSMAcc <- as.data.frame(do.call(rbind, PSSMAcc))
rownames(PSSMAcc) <- names(seqs)

PSSMFeature <- lapply(pssmmat,extractPSSMFeature)
PSSMFeature <- as.data.frame(do.call(rbind, PSSMFeature))
rownames(PSSMFeature) <- names(seqs)

BLOSUM <- lapply(seqs, extractBLOSUM, submat = "AABLOSUM62", k = 5, lag=7)
BLOSUM <- as.data.frame(do.call(rbind, BLOSUM))
rownames(BLOSUM) <- names(seqs)

DescScales <- lapply(seqs, extractDescScales,propmat="AATopo",pc=5,lag=7)
DescScales <- as.data.frame(do.call(rbind, DescScales))
rownames(DescScales) <- names(seqs)

protfp <- lapply(seqs,extractProtFP, pc=5, lag=7, scale = TRUE, silent = TRUE)
protfp <- as.data.frame(do.call(rbind,protfp))
rownames(protfp) <- names(seqs)

features <- cbind(AAC, DC, TC, Moreau, CTDC, CTDT, CTDD, SOCN, QSO, CTriad, PAAC, APAAC, PSSMAcc, PSSMFeature, BLOSUM, DescScales, protfp)
write.table(features, 'R_features.tsv', col.names = NA, quote = FALSE, sep = "\t")
